# Per Chris Zimmer:
#A hostname takes the form of [A-H][01-36]n[01-18]
#The first portion Letter [A-H] + number [01 - 36] represents the rack our node sits in
#The letter portion is the floor row
#The number portion is the floor column 
#n[01-18] is the particular node within the rack. (1 switch hop)
#Neighborhoods correspond to groups of 18 racks - so
#A01-A18 are connected in an 3 hop neighborhood
#A19-A36 are in a separate 3 hop neighborhood
import re
import argparse

def calcSwitchHops(n1, n2):
	m1 = re.match(r"([a-h])([0-9]+)(n)([0-9]+)", n1)	
	m2 = re.match(r"([a-h])([0-9]+)(n)([0-9]+)", n2)	
	if m1 == None or m2 == None:
		print("Error with names: {}, {}".format(n1,n2))
		return
	m1 = m1.groups()
	m2 = m2.groups()
	if len(m1) < 4 or len(m2) < 4:
		print("Error with names: {}, {}".format(n1,n2))
		return
	else:
		if m1[0] == m2[0]:
			if m1[1] == m2[1]:
				return 1
			#Check if they are connected via three hop neighborhood
			elif (int(m1[1]) <= 18 and int(m2[1]) <= 18) or (int(m1[1]) > 18 and int(m2[1]) > 18):
				return 2
			else:
				return 3
		else:
			return 3

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("--filein", "-f", help="file containing two Summit hostnames")

	# read arguments from the command line
	args = parser.parse_args()

	if args.filein:
		print("Reading host file %s" % args.filein)
	else:
		print("Needing to include an input file (-f)")
		return

	with open(args.filein, 'r') as fd:
		n1 = fd.readline()
		n2 = fd.readline()
		print(calcSwitchHops(n1,n2))

if __name__ == "__main__":
	main()
